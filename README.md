ML project template 
==============================

An example of the ML project template.

Instructions
------------
1. Clone the repo.
1. Run `make build` to create the missing parts of the directory structure described below. 
1. *Optional:* Run `make virtualenv` to create a python virtual environment. Skip if using conda or some other env manager.
    1. Run `source env/bin/activate` to activate the virtualenv. 
1. Run `make requirements` to install required python packages.
 
Project Structure
------------

    ├── data
    │   ├── processed
    │   ├── raw
    │   ├── samples          
    │   └── datasets
    │
    ├── reports
    │   └── `report_name` 
    │       └── figures
    │       └── tables
    │       └── sections
    │
    ├── results
    │   ├── checkpoints
    │   ├── logs
    │   │   ├── tensorboard
    │   │   └── base_logger
    │   │
    │   └── models
    │  
    ├── src
    │   ├── data_processing          
    │   │   ├── analysis    
    │   │   ├── cleaning
    │   │   ├── feature_extraction  
    │   │   └── scripts
    │   │
    │   ├── datasets
    │   │   ├── `dataset_type`
    │   │   │   └── `dataset_name`
    │   │   │       └── make_dataset.py
    │   │   │
    │   │   └── scripts
    │   │   
    │   ├── models
    │   │   └── `model_type`
    │   │       └── `model_name`
    │   │           └── model.py
    │   │   
    │   ├── learning
    │   │   └── `learning_type`
    │   │       └── `learning_name`
    │   │           └── train.py
    │   │   
    │   ├── predictors
    │   │   └── `predictor_type`
    │   │       └── `predictor_name`
    │   │           └── predict.py
    │   │
    │   ├── experiments
    │   │   ├── `experiment_type`
    │   │   │   └── `experiment_name`
    │   │   │       ├── config.json
    │   │   │       └── experiment_1.py
    │   │   │
    │   │   ├── commons
    │   │   │   └── experiments_base.py
    │   │   │
    │   │   └── scripts
    │   │
    │   ├── visualizations
    │   │   ├── notebooks
    │   │   │   ├── data_processing
    │   │   │   │   ├── 01-`notebook-name`.ipynb
    │   │   │   │   └── 02-`notebook-name`.ipynb
    │   │   │   │
    │   │   │   ├── `dataset_name`
    │   │   │   ├── `model_name`
    │   │   │   └── commons
    │   │   │
    │   │   ├── plots.py
    │   │   └── tables.py
    │   │
    │   ├── utils
    │   │
    │   ├── settings.py
    │   └── user_settings.py
    │
    ├── tests
    │   └── `package_name` 
    │       └── test_`module_name`.py
    │
    ├── .gitignore
    ├── .gitlab-ci.yml
    ├── LICENSE
    ├── Makefile
    ├── README.md
    ├── requirements.txt
    ├── requirements-dev.txt
    ├── setup.cfg
    └── tox.ini
    