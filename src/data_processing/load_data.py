import os

import pandas as pd
from sklearn.datasets import load_iris as _load_iris
from sklearn.preprocessing import StandardScaler

from src.settings import DATA_DIR


def load_iris(standardize=False):
    data = _load_iris(as_frame=True)
    df = data['data']
    df['target'] = data['target']
    return _load_data(df, standardize)


def load_glass(standardize=False):
    df = pd.read_csv(os.path.join(DATA_DIR, 'raw', 'glass', 'glass.csv'))
    return _load_data(df, standardize)


def load_wine(standardize=False):
    df = pd.read_csv(os.path.join(DATA_DIR, 'raw', 'wine', 'wine.csv'))
    return _load_data(df, standardize)


def load_seeds(standardize=False):
    df = pd.read_csv(os.path.join(DATA_DIR, 'raw', 'seeds', 'seeds.csv'))
    return _load_data(df, standardize)


def _load_data(df, standardize):
    if standardize:
        df[df.columns[:-1]] = _standardize(df[df.columns[:-1]])
    return df, df[df.columns[:-1]].to_numpy(), df[df.columns[-1]].to_numpy()


def _standardize(x):
    return StandardScaler().fit_transform(x)
