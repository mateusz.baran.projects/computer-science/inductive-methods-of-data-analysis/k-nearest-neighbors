from collections import defaultdict
from functools import partial
from multiprocessing import Pool

import numpy as np
from sklearn.metrics import f1_score
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from tqdm.auto import tqdm

from src.utils.experiments import param_generator


def _run(X, y, params):
    means = []
    stds = []
    for random_state in range(params['RandomStates']):
        f_scores = []
        skf = StratifiedKFold(n_splits=params['N-splits'], shuffle=True, random_state=random_state)
        for train_index, test_index in skf.split(X, y):
            X_train = X[train_index]
            y_train = y[train_index]
            X_test = X[test_index]
            y_test = y[test_index]

            if params['K-neighbors'] > len(y_train):
                return {}

            model = KNeighborsClassifier(n_neighbors=params['K-neighbors'], p=params['Minkowski-p'],
                                         weights=params['Weight'])
            model.fit(X_train, y_train)
            y_pred = model.predict(X_test)

            f_scores.append(f1_score(y_test, y_pred, zero_division=0, average='macro'))

        means.append(np.mean(f_scores))
        stds.append(np.std(f_scores))

    history = {}
    for k, v in params.items():
        history[k] = str(v)

    mean, std = np.mean(means), np.std(stds)
    history['F1 mean'] = mean
    history['F1 std'] = std
    history['F1'] = f'{mean:.1%} ± {std:.1%}'

    return history


def run(X, y, param_dict):
    results = []
    tasks = list(param_generator(param_dict))

    with Pool() as p:
        for result in tqdm(p.imap_unordered(partial(_run, X, y), tasks), total=len(tasks)):
            results.append(result)

    history = defaultdict(list)
    for to_merge in results:
        for key, value in to_merge.items():
            history[key].append(value)

    return history
