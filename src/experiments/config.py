from src.utils.weighting import GaussKernel
import numpy as np


def get_config(n_splits=None, random_state=None, weights=None, n_neighbors=None, p=None):
    return {
        'N-splits': [5, 7, 9] if n_splits is None else n_splits,
        'RandomStates': [30] if random_state is None else random_state,
        'Weight': ['uniform', 'distance', GaussKernel()] if weights is None else weights,
        'K-neighbors': [1, 2, 3, 4, 5] if n_neighbors is None else n_neighbors,
        'Minkowski-p': [1, 2, np.inf] if p is None else p,
    }
