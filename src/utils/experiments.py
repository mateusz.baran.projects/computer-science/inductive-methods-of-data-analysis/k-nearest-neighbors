from itertools import product


def param_generator(param_dict):
    for params in product(*param_dict.values()):
        yield {k: v for k, v in zip(param_dict.keys(), params)}
