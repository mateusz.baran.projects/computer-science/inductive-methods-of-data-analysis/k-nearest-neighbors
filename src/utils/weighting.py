import numpy as np


class GaussKernel:
    def __call__(self, d):
        return np.exp(- (d ** 2) / 2) / np.sqrt(2 * np.pi)

    def __repr__(self):
        return 'gauss'

    def __str__(self):
        return 'gauss'
